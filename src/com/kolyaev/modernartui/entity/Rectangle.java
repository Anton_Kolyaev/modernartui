package com.kolyaev.modernartui.entity;

import android.graphics.Color;
import android.graphics.Rect;

public class Rectangle {

	private int[] colorStart;
	private int[] colorEnd;
	private Rect rect;

	private static int maxPos;

	public Rectangle(int[] colorStart, int[] colorEnd) {
		super();
		this.colorStart = colorStart;
		this.colorEnd = colorEnd;
		// rect = new Rect(10, 10, 150, 400);
	}

	public static void setMaxPos(int pos) {
		maxPos = pos;
	}

	public void setRect(Rect rect) {
		this.rect = rect;

	}

	public int[] getRGB(int pos) {
		if (pos == 0)
			return colorStart;
		if (maxPos == pos)
			return colorEnd;
		int[] color = new int[3];
		for (int i = 0; i < 3; i++) {
			color[i] = colorStart[i] + (colorEnd[i] - colorStart[i]) * (pos)
					/ maxPos;
		}

		return color;
	}

	public int getColor(int pos) {
		int[] c = getRGB(pos);
		return Color.argb(255, c[0], c[1], c[2]);
	}

	public Rect getRect() {
		return rect;
	}

}
