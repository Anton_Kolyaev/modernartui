package com.kolyaev.modernartui.entity;

import java.util.List;
import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;

public class DrawThread extends Thread {
	private boolean running = false;
	private SurfaceHolder surfaceHolder;
	private int pos;
	private List<Rectangle> rectangles;
	private Paint paint;
	private final static String TAG = "DrawThread";
	private boolean firstRun = true;
	private int h;
	private int w;

	public DrawThread(SurfaceHolder surfaceHolder) {
		this.surfaceHolder = surfaceHolder;
		paint = new Paint();

	}

	public void setRunning(boolean running) {
		this.running = running;
		Log.d(TAG, "Draw thread is running");
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public void setRectangles(List<Rectangle> rectangles) {
		this.rectangles = rectangles;
	}

	@Override
	public void run() {
		Canvas canvas;
		while (running) {
			canvas = null;
			try {
				canvas = surfaceHolder.lockCanvas(null);
				if (canvas == null)
					continue;
				if (firstRun) {
					firstRun = false;
					w = canvas.getWidth();
					h = canvas.getHeight();
					firstInit();
				}

				// ����� ������...
				draw(canvas);

			} finally {
				if (canvas != null) {
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

	private void firstInit() {

		Random rnd = new Random();
		int xStart;
		int xEnd;
		int yStart;
		int yEnd;
		int buf;
		for (Rectangle r : rectangles) {
			xStart = rnd.nextInt(w - 50);
			xEnd = rnd.nextInt(w - 50) + 50;
			yStart = rnd.nextInt(h - 50);
			yEnd = rnd.nextInt(h - 50) + 50;
			if (xStart > xEnd) {
				buf = xStart;
				xStart = xEnd;
				xEnd = buf;
			}
			if (yStart > yEnd) {
				buf = yStart;
				yStart = yEnd;
				yEnd = buf;
			}
			r.setRect(new Rect(xStart, yStart, xEnd, yEnd));

		}
	}

	private void draw(Canvas canvas) {

		for (Rectangle r : rectangles) {
			paint.setColor(r.getColor(pos));
			canvas.drawRect(r.getRect(), paint);
		}

	}
}
