package com.kolyaev.modernartui.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class RectangleView extends SurfaceView implements
		SurfaceHolder.Callback {

	private DrawThread drawThread;

	private Random rnd;
	private int xStart;
	private int xEnd;
	private int yStart;
	private int yEnd;
	private int w;
	private int h;
	private boolean firstRun;

	private List<Rectangle> rectangles;

	public RectangleView(Context c, int maxpos, int amount) {
		super(c);
		getHolder().addCallback(this);
		rnd = new Random();
		firstRun = true;
		rectangles = new ArrayList<Rectangle>();
		for (int i = 0; i < amount; i++) {
			rectangles.add(new Rectangle(initColor(), initColor()));
		}
		rectangles.add(new Rectangle(new int[] { 255, 255, 255, 255 },
				new int[] { 255, 255, 255, 255 }));
		Rectangle.setMaxPos(maxpos);

	}

	@Override
	protected void onDraw(Canvas canvas) {

		w = canvas.getWidth();
		h = canvas.getHeight();
		if (w != 0 && h != 0)
			if (firstRun) {
				firstRun = false;
				Log.d("RA", "w = " + w + ", h = " + h);

				for (Rectangle r : rectangles) {
					xStart = rnd.nextInt(w - 50);
					xEnd = rnd.nextInt(w - 50) + 50;
					yStart = rnd.nextInt(h - 50);
					yEnd = rnd.nextInt(h - 50) + 50;
					int buf;
					if (xStart > xEnd) {
						buf = xStart;
						xStart = xEnd;
						xEnd = buf;
					}
					if (yStart > yEnd) {
						buf = yStart;
						yStart = yEnd;
						yEnd = buf;
					}
					r.setRect(new Rect(xStart, yStart, xEnd, yEnd));

				}
				drawThread.setRectangles(rectangles);
				drawThread.setRunning(true);
				drawThread.start();
				// draw(0);
			}
	}

	public void update(int position) {
		if (null != drawThread)
		drawThread.setPos(position);
	}

	// ��������� ����� �����������
	private int[] initColor() {
		int[] res = { rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255) };
		return res;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		drawThread = new DrawThread(getHolder());
		drawThread.setRectangles(rectangles);
		drawThread.setRunning(true);
		drawThread.start();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// Auto-generated method stub

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Auto-generated method stub
		stopDraw();

	}

	@Override
	// stop drawing thread if back button pressed
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			stopDraw();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		//  Auto-generated method stub
		stopDraw();	
		super.onConfigurationChanged(newConfig);
		
	}

	private void stopDraw() {
		boolean retry = true;
		drawThread.setRunning(false);
		while (retry) {
			try {
				drawThread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}

}
